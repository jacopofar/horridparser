package pcfg;

import java.util.Collections;
import java.util.List;

/**
 * A rule of a Grammar in one of these forms:
 * * TERMINALS: a string of terminal characters
 * * NON TERMINALS: a sequence of non-terminal symbols
 * It's NOT in Chomsky Normal Form, since the right part can consist in one or more than two non-terminal symbols,
 * but a CNF grammar can be written this way, using an empty string to represent the rule S -> epsilon.
 * */
public final class GrammarRule {
	private NonTerminalSymbol left;
	private boolean isTerminal;
	private double value;
	private String terminals=null;
	private List<NonTerminalSymbol> nonTerminals;
	
	public GrammarRule(double val,NonTerminalSymbol nts,String terminals){
		this.terminals=terminals;
		left=nts;
		isTerminal=true;
		value=val;
	}
	public GrammarRule(double val,NonTerminalSymbol nts,List<NonTerminalSymbol> terminals){
		nonTerminals=Collections.unmodifiableList(terminals);
		left=nts;
		isTerminal=false;
		value=val;
	}
	public boolean isTerminal() {
		return isTerminal;
	}
	/**
	 * Return the matching String, if this rule is terminal  null otherwise
	 * */
	public String getMatchingString() {
		return terminals;
	}
	
	/**
	 * Return the probability value, a number between 0 and 1
	 * */
	public double getValue() {
		return value;
	}
	
	public int hashCode(){
		return left.hashCode()+(terminals!=null ? terminals.hashCode():nonTerminals.hashCode())+Double.toString(value).hashCode();
	}
	
	public boolean equals(Object o){
		if(!(o instanceof GrammarRule)) return false;
		
		return false;
	}
	
}