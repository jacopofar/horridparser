package pcfg;

import java.util.List;

/**
 * Defines a node of a tree parsed from a text using a CFPG
 * */
public final class Node {
	private double value;
	private GrammarRule rule;
	private int spanStart;
	private int spanEnd;
	private List<Node> sons;
	private int hashCode=-100;
	public Node(int spanStart, int spanEnd, double value, GrammarRule rule){
		this.spanStart=spanStart;
		this.spanEnd=spanEnd;
		this.value=value;
		this.rule=rule;
	}
	public int hashCode(){
		if(hashCode==-100)
			hashCode=spanStart*17+spanEnd+sons.hashCode()+rule.hashCode()+Double.toString(value).hashCode();
		return hashCode;
	}
	
	public boolean equals(Object o){
		if(!(o instanceof Node)) return false;
		if(this.spanStart!=((Node)o).spanStart) return false;
		if(this.spanEnd!=((Node)o).spanEnd) return false;
		if(!this.sons.equals(((Node)o).sons)) return false;
		if(!this.rule.equals(((Node)o).rule)) return false;
		return true;
		
	}
}
