package pcfg;

/**
 * Describe a non-terminal symbol, it's immutable
 * */
public final class NonTerminalSymbol {
	private String name;
	public NonTerminalSymbol(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
	public int hashCode(){
		return name.hashCode();
	}
	public boolean equals(Object o){
		if(o instanceof NonTerminalSymbol)
			return ((NonTerminalSymbol)o).name.equals(name);
		else
			return false;
	}
}
