package pcfg;

import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Defines a Probabilistic Context-Free Grammar and allows to parse strings using CYK algorithm
 * */
public class PCFG {
	private Vector<GrammarRule> rules=new Vector<GrammarRule>();
	/**
	 * Adds a rule checking whether it creates a cycle in the grammar.
	 */
	public void addRule(GrammarRule g){
		//TODO: check for cycles. Now it doesn't happen, the parsing may never end!
		rules.add(g);
	}
	/**
	 * Returns a list of root nodes of the matching trees.
	 * It returns the 
	 * */
	public List<Node> parse(String text){
		//the list of matching trees root until now
		HashSet<Node> found=new HashSet<Node>();
		//first step: let's identify all the terminal rules, that is, all the marching explicit strings
		for(GrammarRule g:rules){
			if(!g.isTerminal()) continue;
			Pattern p = Pattern.compile(g.getMatchingString());
			Matcher m = p.matcher(text);
			while(m.find()){
				//found a match, let's create a corresponsing node
				found.add(new Node(m.start(),m.start()+g.getMatchingString().length(),g.getValue(),g));
			}
		}
		return null;
	}
}
